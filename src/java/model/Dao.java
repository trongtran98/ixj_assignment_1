/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author TrongTran
 */
public class Dao {

    private Connection conn;

    public Dao() {
        try {
            Class.forName("org.apache.derby.jdbc.ClientDriver");
            conn = DriverManager.getConnection("jdbc:derby://localhost:1527/asm_customer", "sa", "123456");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ResultSet getData() {
        ResultSet rs = null;
        try {
            Statement stmt = conn.createStatement();
            rs = stmt.executeQuery("select * from Cumtomer");

        } catch (Exception e) {
            e.printStackTrace();
        }
        return rs;
    }

    public void writeXML(String url) {
        try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document document = builder.newDocument();
            Element root = document.createElement("Customers");
            document.appendChild(root);
            ResultSet rs = this.getData();
            while (rs.next()) {
                Element child = document.createElement("Customer");
                root.appendChild(child);

                Element ContactName = document.createElement("ContactName");
                ContactName.setTextContent(rs.getString("ContactName"));
                child.appendChild(ContactName);

                Element ContactTitle = document.createElement("ContactTitle");
                ContactTitle.setTextContent(rs.getString("ContactTitle"));
                child.appendChild(ContactTitle);

                Element CompanyName = document.createElement("CompanyName");
                CompanyName.setTextContent(rs.getString("CompanyName"));
                child.appendChild(CompanyName);

                Element Address = document.createElement("Address");
                Address.setTextContent(rs.getString("Address"));
                child.appendChild(Address);

                Element City = document.createElement("City");
                City.setTextContent(rs.getString("City"));
                child.appendChild(City);

                Element PostalCode = document.createElement("PostalCode");
                PostalCode.setTextContent(rs.getString("PostalCode"));
                child.appendChild(PostalCode);

                Element Country = document.createElement("Country");
                Country.setTextContent(rs.getString("Country"));
                child.appendChild(Country);

                Element Phone = document.createElement("Phone");
                Phone.setTextContent(rs.getString("Phone"));
                child.appendChild(Phone);

                Element Fax = document.createElement("Fax");
                Fax.setTextContent(rs.getString("Fax"));
                child.appendChild(Fax);

            }
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
            transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty(OutputKeys.ENCODING, "iso-8859-1");
            StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);
            DOMSource source = new DOMSource(document);
            transformer.transform(source, result);
            FileWriter fwriter = new FileWriter(new File(url + "customer.xml"));
            fwriter.write(writer.toString());
            fwriter.close();
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(Dao.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(Dao.class.getName()).log(Level.SEVERE, null, ex);
        } catch (TransformerConfigurationException ex) {
            Logger.getLogger(Dao.class.getName()).log(Level.SEVERE, null, ex);
        } catch (TransformerException ex) {
            Logger.getLogger(Dao.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Dao.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
