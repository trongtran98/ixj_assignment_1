/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import entity.Customer;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import nu.xom.Builder;
import nu.xom.Document;
import nu.xom.Node;
import nu.xom.Nodes;
import nu.xom.ParsingException;
import nu.xom.ValidityException;
import nux.xom.xquery.XQueryUtil;

/**
 *
 * @author TrongTran
 */
public class ProcessingXML {

    public ProcessingXML() {
    }

    public ArrayList readXML(String url) throws ParsingException, ValidityException, IOException {
        Document document = new Builder().build(new File(url + "Customers.xml"));
        Nodes nodes = XQueryUtil.xquery(document, "//Customer");
        ArrayList arrayList = new ArrayList();
        System.out.println(nodes.size());
        for (int i = 0; i < nodes.size(); i++) {
            Customer cus = new Customer();
            Node node = nodes.get(i);
            cus.setContactName(node.getChild(1).getValue());
            cus.setContactTitle(node.getChild(3).getValue());
            cus.setCompanyName(node.getChild(5).getValue());
            cus.setAddress(node.getChild(7).getValue());
            cus.setCity(node.getChild(9).getValue());
            cus.setPostalCode(Integer.parseInt(node.getChild(11).getValue()));
            cus.setCountry(node.getChild(13).getValue());
            cus.setPhone(node.getChild(15).getValue());
            cus.setFax(node.getChild(17).getValue());

            arrayList.add(cus);
        }
        return arrayList;
    }

    public ArrayList filtedContent(String url, String filter) throws ParsingException, ValidityException, IOException {
        Document document = new Builder().build(new File(url + "customer.xml"));
        Nodes nodes = XQueryUtil.xquery(document, "//Customer[City = '"+filter+"']");
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < nodes.size(); i++) {
            Customer cus = new Customer();
            Node node = nodes.get(i);
            cus.setContactName(node.getChild(1).getValue());
            cus.setContactTitle(node.getChild(3).getValue());
            cus.setCompanyName(node.getChild(5).getValue());
            cus.setAddress(node.getChild(7).getValue());
            cus.setCity(node.getChild(9).getValue());
            cus.setPostalCode(Integer.parseInt(node.getChild(11).getValue()));
            cus.setCountry(node.getChild(13).getValue());
            cus.setPhone(node.getChild(15).getValue());
            cus.setFax(node.getChild(17).getValue());
            arrayList.add(cus);
        }
        return arrayList;
    }
    
}
