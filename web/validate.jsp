<%-- 
    Document   : validate
    Created on : May 26, 2018, 4:54:28 PM
    Author     : TrongTran
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Validate Page</title>
    </head>
    <body>
        <h4>${errors}</h4>
    </body>
</html>
