<%-- 
    Document   : index
    Created on : May 25, 2018, 4:34:31 PM
    Author     : TrongTran
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form action="ExportData">
            <input type="submit" value="Export Data"/>
        </form>
        <form action="ValidateData">
            <input type="submit" value="Validate Data"/>
        </form>
        <form action="ShowData" method="get">
            <input name="filter" type="search" placeholder="input sth..."/>
            <input type="submit" value="Show Data"/>
        </form>
    </body>
</html>
